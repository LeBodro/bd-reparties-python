import sqlite3
import os


if os.access('spells.db', os.F_OK):
    os.remove('spells.db')
_connexion = sqlite3.connect('spells.db')
_cursor = _connexion.cursor()


def initialize():
    _cursor.execute('''CREATE TABLE spells
             (name text, level int, verbal int, somatic int, material int, focus int, resistance int)''')


def insert(spell_info):
    _connexion.execute('INSERT INTO spells VALUES (?, ?, ?, ?, ?, ?, ?)', (
        spell_info['name'],
        spell_info['level'],
        1 if 'V' in spell_info['components'] else 0,
        1 if 'S' in spell_info['components'] else 0,
        1 if 'M' in spell_info['components'] else 0,
        1 if 'F' in spell_info['components'] else 0,
        1 if spell_info['spell_resistance'] else 0
    ))
    _connexion.commit()


def query():
    print('SQL QUERY')
    _cursor.execute('SELECT * FROM spells WHERE level<=4 AND verbal=1 AND somatic=0 AND material=0 AND focus=0')
    rows = _cursor.fetchall()
    for row in rows:
        print(row)


def close():
    _cursor.execute('drop table if exists spells')
    _connexion.close()
