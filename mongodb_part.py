from pymongo import MongoClient
from bson import Code


client = MongoClient()
db = client.spells
db.things.delete_many({})


def insert_spells(spells):
    db.things.insert_many(spells)


def insert(spell):
    db.things.insert(spell)


def map_reduce():
    print('MONGODB MAP REDUCE')
    _map = Code("function () {"
                "  emit(this.level, {spells: [{"
                "    name: this.name, components: this.components"
                "  }]});"
                "}")

    _reduce = Code("function (key, values) {"
                   "  var reduced_object = {spells: []};"
                   "  if (key <= 4) {"
                   "    for (var j=0; j < values.length; j++) {"
                   "      for (var i = 0; i < values[j]['spells'].length; i++) {"
                   "        if(values[j]['spells'][i].components.length == 1 && values[j]['spells'][i].components.includes('V'))"
                   "          reduced_object['spells'].push(values[j]['spells'][i]);"
                   "      }"
                   "    }"
                   "  }"
                   "  return reduced_object;"
                   "}")

    result = db.things.map_reduce(_map, _reduce, "results")
    for doc in result.find():
        print(doc)

