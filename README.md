# BD Réparties - Exercice 1

This project is a homework for which the goal was to learn using map-reduce with MongoDB.
You will need MongoDB to be installed and to have a MongoDB server running for this project to work.
Refer to MongoDB's documentation for further details about setting up your server.

## You will need

- Python
- Virtualenv
- Virtualenvwrapper (or virtualenvwrapper-win on Windows)

## Setup Project

1. Make virtual env: `mkvirtualenv environment-name -a .` (from project folder)
2. Install requirements: `pip install -r requirements.txt`
3. Make sure everything works: `python main.py`

## Useful Commands

- Activate virtual environment on Windows: `workon environment-name`
- Intall new requirements after pulling changes: `pip install -r requirements.txt`
- Installing a _pip_ package: `pip install package-name`
- Adding installed package to requirements file: `pip freeze > requirements.txt`
- Execute a python script from file: `python python-file-to-execute.py`
- Deactivate virtual environment on Windows: `deactivate`
